// Select DOM Items
const createEl = document.createElement.bind(document); // alias for creating an element
const getEl = document.getElementById.bind(document);
const navlinks = document.querySelectorAll(".nav-links");

function listen(eventType, selector, func) {
  document.addEventListener(eventType, function (event) {
    if (event.target.webkitMatchesSelector(selector)) {
      func.call(event.target, event);
    }
  }, false);
}

function displayNav() {
  let navLinks =  document.querySelector('.nav-links');
  let icons =  document.querySelector('.icons');
  setTimeout (function (){
    navLinks.style.display = 'block';
    if(document.querySelector('.icons')) {
      icons.style.display = 'block';
      }
  }, 400)
}

function hideNav() {
  let navLinks =  document.querySelector('.nav-links');
  let icons =  document.querySelector('.icons');
  navLinks.style.display = 'none';
  if(document.querySelector('.icons')) {
  icons.style.display = 'none';
  }
}
/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
  getEl("mySidenav").style.width = "300px";
  if (getEl('home')) {
    getEl("home").style.marginLeft = "300px";
    displayNav();
  } else if (getEl('about')) {
    getEl("about").style.marginLeft = "300px";
    displayNav();
  } else if (getEl('work')) {
    getEl("work").style.marginLeft = "300px";
    displayNav();
  } else if(getEl('contact')) {
    getEl("contact").style.marginLeft = "300px";
    displayNav();
  }
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
  let navLinksDisplay =  document.querySelector('.nav-links').style.display;
  getEl("mySidenav").style.width = "0px";
  if (getEl('home')) {
    hideNav();
    getEl("home").style.marginLeft = "17.4%";
  } else if (getEl('about')) {
    hideNav();
    getEl("about").style.marginLeft = "17.4%";
  } else if (getEl('work')) {
    hideNav();
    getEl("work").style.marginLeft = "17.4%";
  } else if(getEl('contact')) {
    hideNav();
    getEl("contact").style.marginLeft = "17.4%";
  }
}

listen('click', '.hamburger-container, .hamburger-links, .link', function () {
  openNav();
});

listen('click', '#home, #contact, #work, #about, header, .lg-heading ', function(e) {
  if(!e.target.classList.contains('closebtn')) {
    closeNav();
  }
});

listen('click', '.closebtn', function () {
  closeNav();
});

